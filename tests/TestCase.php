<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function setUp(): void
    {
        parent::setUp();

        $this->app = $this->createApplication();
    }
}
