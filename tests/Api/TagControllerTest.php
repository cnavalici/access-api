<?php

namespace Tests\Api;

use App\Services\AccessService;
use App\Services\DoorService;
use App\Services\InMemoryAccessService;
use App\Services\InMemoryDoorService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tests\ApiTestCase;
use TiMacDonald\Log\LogFake;

class TagTest extends ApiTestCase
{
    const TAG01_ID = "48efcdad-64b0-11eb-8bb8-3cf011260500";
    const TAG02_ID = "0fa0d17e-6531-11eb-8bb8-3cf0112605d4";
    const TAG03_ID = "6ca1e28e-1234-11eb-8bb8-3cf0112605d1";
    const TAG04_ID = "93ff881b-662f-11eb-a8bf-3cf0112605d4";

    const ACCOUNT_ID = "3ac8504f-64ae-11eb-8bb8-3cf0112605d4";

    const DOOR03_ID = "03";
    const DOOR01_ID = "01";

    public function setUp(): void
    {
        parent::setUp();

        Log::swap(new LogFake());

        $this->app->bind(AccessService::class, function () {
            return (new InMemoryAccessService())
                ->withAccountForTag(self::TAG01_ID, self::ACCOUNT_ID)
                ->withAccountForTag(self::TAG02_ID, self::ACCOUNT_ID)
                ->withAccountForTag(self::TAG03_ID, self::ACCOUNT_ID)
                ->withAccessForTagOnDoor(self::DOOR01_ID, self::TAG01_ID, false)
                ->withAccessForTagOnDoor(self::DOOR01_ID, self::TAG02_ID, true)
                ->withAccessForTagOnDoor(self::DOOR03_ID, self::TAG01_ID, true);
        });

        $this->app->bind(DoorService::class, function () {
            return (new InMemoryDoorService())
                ->withDoorWorking(self::DOOR01_ID)
                ->withDoorBlocked(self::DOOR03_ID);
        });
    }

    public function testTagNotRegistered()
    {
        $jsonResponse = $this->actingAsAdmin()->json('GET', '/access/'.self::DOOR01_ID.'/'.self::TAG04_ID);

        $jsonResponse
            ->assertStatus(401)
            ->assertJson([
                "message" => "Tag is not assigned with this account",
                "statusCode" => 401
            ]);
    }

    public function testDoorBlocked()
    {
        $jsonResponse = $this->actingAsAdmin()->json('GET', '/access/'.self::DOOR03_ID.'/'.self::TAG01_ID);

        $jsonResponse
            ->assertStatus(403)
            ->assertJson([
                "message" => "Unit not available",
                "statusCode" => 403
            ]);
    }

    public function testTagDeniedOnEnabledDoor()
    {
        $jsonResponse = $this->actingAsAdmin()->json('GET', '/access/'.self::DOOR01_ID.'/'.self::TAG01_ID);

        $jsonResponse
            ->assertStatus(403)
            ->assertJson([
                "message" => "Denied",
                "statusCode" => 403
            ]);

        Log::setCurrentChannel('access');
        Log::assertLogged('info', function ($message, $context) {
            return Str::contains($message, '"access":false');
        });
    }

    public function testTagAllowedOnEnabledDoor()
    {
        $jsonResponse = $this->actingAsAdmin()->json('GET', '/access/'.self::DOOR01_ID.'/'.self::TAG02_ID);

        $jsonResponse
            ->assertStatus(200)
            ->assertJson([
                "message" => "Granted",
                "statusCode" => 200
            ]);

        Log::setCurrentChannel('access');
        Log::assertLogged('info', function ($message, $context) {
            return Str::contains($message, '"access":true');
        });
    }

    public function testTagRegisteredButNotAssignedWithDoor()
    {
        $jsonResponse = $this->actingAsAdmin()->json('GET', '/access/'.self::DOOR01_ID.'/'.self::TAG03_ID);

        $jsonResponse
            ->assertStatus(404)
            ->assertJson([
                "message" => "The specified tag ".self::TAG03_ID." is not registered with the door ".self::DOOR01_ID,
                "statusCode" => 404
            ]);

        Log::setCurrentChannel('access');
        Log::assertLogged('warning', function ($message, $context) {
            return Str::contains($message, '"access":false');
        });
    }

    public function testPublicAccessOrInvalidCredentials()
    {
        $jsonResponse = $this->json('GET', '/access/'.self::DOOR01_ID.'/'.self::TAG03_ID);
        $jsonResponse->assertStatus(401);
    }
}
