<?php

use Illuminate\Support\Facades\Hash;

class UserSeeder extends BaseSeeder
{
    const USER_EMAIL = 'dev@saltoks.com';
    const USER_PASSWORD = '123456';
    const USER_ID = "3ac8504f-64ae-11eb-8bb8-3cf0112605d4";
    const USER_NAME = "jbravo";
    const USER_FULLNAME = "Johnny Bravo";

    public function runFake()
    {
        DB::table('users')->insert([
            'name' => self::USER_FULLNAME,
            'username' => self::USER_NAME,
            'email' => self::USER_EMAIL,
            'password' => Hash::make(self::USER_PASSWORD),
            'user_id' => self::USER_ID
        ]);
    }
}
