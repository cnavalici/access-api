<?php

namespace App\Providers;

use App\Exceptions\ApiExceptionHandler;
use App\Services\AccessService;
use App\Services\DbLoggingService;
use App\Services\DoorService;
use App\Services\LoggingService;
use App\Services\RedisAccessService;
use App\Services\RedisDoorService;
use Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerExceptionHandler();
        $this->registerTelescope();
        $this->registerAccessService();
        $this->registerDoorsService();
        $this->registerLoggingService();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the exception handler - extends the Dingo one
     *
     * @return void
     */
    protected function registerExceptionHandler()
    {
        $this->app->singleton('api.exception', function ($app) {
            return new ApiExceptionHandler($app['Illuminate\Contracts\Debug\ExceptionHandler'], Config('api.errorFormat'), Config('api.debug'));
        });
    }

    /**
     * Conditionally register the telescope service provider
     */
    protected function registerTelescope()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Register REDIS access service
     */
    protected function registerAccessService()
    {
        $this->app->bind(AccessService::class, RedisAccessService::class);
    }

    /**
     * Register REDIS doors service
     */
    protected function registerDoorsService()
    {
        $this->app->bind(DoorService::class, RedisDoorService::class);
    }

    /**
     * Register Logging Service
     */
    protected function registerLoggingService()
    {
        $this->app->bind(LoggingService::class, DbLoggingService::class);
    }
}
