<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Redis;

class RedisAccessService implements AccessService
{
    const ACCOUNT_TAGS = 'account_tags';

    /**
     * @param string $doorId
     * @param string $uuid
     * @throws \Exception
     * @return bool
     */
    public function accessForTagOnDoor(string $doorId, string $uuid): bool
    {
        $doorSetIdentifier = $this->doorSetIdentifier($doorId);

        $access = Redis::hMget($doorSetIdentifier, $uuid)[0];

        if ($access === null) {
            throw new \Exception(sprintf("The specified tag %s is not registered with the door %s", $uuid, $doorId));
        }

        return (bool)$access;
    }

    /**
     * @param string $uuid
     * @return string|null
     */
    public function accountForTag(string $uuid): ?string
    {
        return Redis::hMget(self::ACCOUNT_TAGS, $uuid)[0];
    }

    /**
     * Creates a full door identifier to match the Redis records
     *
     * @param string $doorId
     * @return string
     */
    protected function doorSetIdentifier(string $doorId): string
    {
        return sprintf("door%s", RedisDoorService::formatDoorId($doorId));
    }
}
