<?php
declare(strict_types=1);

namespace App\Services;

interface DoorService
{
    const DOOR_WORKING = 'working';
    const DOOR_BLOCKED = 'blocked';

    public function isBlocked(string $doorId): bool;

    public function blockDoor(string $doorId): int;

    public function unBlockDoor(string $doorId): int;
}
