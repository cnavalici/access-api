<?php
declare(strict_types=1);

namespace App\Services;

interface LoggingService
{
    public function tagNotFound(string $doorId, string $uuid): void;

    public function tagSuccess(string $doorId, string $uuid): void;

    public function tagDenied(string $doorId, string $uuid): void;
}
