<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Log;

class DbLoggingService implements LoggingService
{
    /**
     * @param string $doorId
     * @param string $uuid
     */
    public function tagNotFound(string $doorId, string $uuid): void
    {
        $logMessage = [
            'doorId' => $doorId,
            'tagId' => $uuid,
            'access' => false
        ];

        Log::channel('access')->warning(json_encode($logMessage));
    }

    /**
     * @param string $doorId
     * @param string $uuid
     */
    public function tagSuccess(string $doorId, string $uuid): void
    {
        $logMessage = [
            'doorId' => $doorId,
            'tagId' => $uuid,
            'access' => true
        ];

        Log::channel('access')->info(json_encode($logMessage));
    }

    /**
     * @param string $doorId
     * @param string $uuid
     */
    public function tagDenied(string $doorId, string $uuid): void
    {
        $logMessage = [
            'doorId' => $doorId,
            'tagId' => $uuid,
            'access' => false
        ];

        Log::channel('access')->info(json_encode($logMessage));
    }
}
