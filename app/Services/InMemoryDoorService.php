<?php
declare(strict_types=1);

namespace App\Services;


class InMemoryDoorService implements DoorService
{
    private $doors = [];

    /**
     * @param string $doorId
     * @return bool
     */
    public function isBlocked(string $doorId): bool
    {
        if (!array_key_exists($doorId, $this->doors)) {
            return false;
        }

        return $this->doors[$doorId] == DoorService::DOOR_BLOCKED;
    }

    /**
     * @param string $doorId
     * @return $this
     */
    public function withDoorBlocked(string $doorId): InMemoryDoorService
    {
        $this->doors[$doorId] = DoorService::DOOR_BLOCKED;

        return $this;
    }

    /**
     * @param string $doorId
     * @return $this
     */
    public function withDoorWorking(string $doorId): InMemoryDoorService
    {
        $this->doors[$doorId] = DoorService::DOOR_WORKING;

        return $this;
    }

    /**
     * @param string $doorId
     * @return int
     */
    public function blockDoor(string $doorId): int
    {
        return 0;
    }

    /**
     * @param string $doorId
     * @return int
     */
    public function unBlockDoor(string $doorId): int
    {
        return 0;
    }
}
