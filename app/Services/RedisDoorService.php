<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Redis;

class RedisDoorService implements DoorService
{
    const DOORS_STATUSES = 'doors_statuses';

    /**
     * @param string $doorId
     * @return bool
     */
    public function isBlocked(string $doorId): bool
    {
        $status = Redis::hMget(self::DOORS_STATUSES, self::formatDoorId($doorId))[0];

        return $status == self::DOOR_BLOCKED;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return Redis::hGetAll(self::DOORS_STATUSES);
    }

    /**
     * @param string $doorId
     * @return int
     */
    public function blockDoor(string $doorId): int
    {
        $doorId = self::formatDoorId($doorId);

        return Redis::hSet(self::DOORS_STATUSES, $doorId, self::DOOR_BLOCKED);
    }

    /**
     * @param string $doorId
     * @return int
     */
    public function unBlockDoor(string $doorId): int
    {
        $doorId = self::formatDoorId($doorId);

        return Redis::hSet(self::DOORS_STATUSES, $doorId, self::DOOR_WORKING);
    }

    /**
     * @param string $doorId
     * @return string
     */
    public static function formatDoorId(string $doorId): string
    {
        return sprintf("%02d", $doorId);
    }
}
