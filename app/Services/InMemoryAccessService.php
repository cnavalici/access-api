<?php
declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Arr;

class InMemoryAccessService implements AccessService
{
    private $accountTags = [];
    private $doorsTags = [];

    /**
     * @param string $doorId
     * @param string $uuid
     * @throws \Exception
     * @return bool
     */
    public function accessForTagOnDoor(string $doorId, string $uuid): bool
    {
        $access = Arr::get($this->doorsTags, "$doorId.$uuid");
        if ($access === null) {
            throw new \Exception(sprintf("The specified tag %s is not registered with the door %s", $uuid, $doorId));
        }

        return $access;
    }

    /**
     * @param string $uuid
     * @return string|null
     */
    public function accountForTag(string $uuid): ?string
    {
        return $this->accountTags[$uuid] ?? null;
    }

    /**
     * @param string $uuid
     * @param string $account
     * @return $this
     */
    public function withAccountForTag(string $uuid, string $account = "3ac8504f-64ae-11eb-8bb8-3cf0112605d4"): InMemoryAccessService
    {
        $this->accountTags[$uuid] = $account;

        return $this;
    }

    /**
     * @param string $doorId
     * @param string $uuid
     * @param bool   $status
     * @return $this
     */
    public function withAccessForTagOnDoor(string $doorId, string $uuid, bool $status = true): InMemoryAccessService
    {
        $this->doorsTags[$doorId][$uuid] = $status;

        return $this;
    }
}
