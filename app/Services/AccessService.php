<?php
declare(strict_types=1);

namespace App\Services;

interface AccessService
{
    public function accessForTagOnDoor(string $doorId, string $uuid): bool;

    public function accountForTag(string $uuid): ?string;
}
