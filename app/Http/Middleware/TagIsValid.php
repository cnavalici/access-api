<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Services\AccessService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Specialtactics\L5Api\Exceptions\UnauthorizedHttpException;

class TagIsValid
{
    /**
     * @var AccessService
     */
    private $accessService;

    public function __construct(AccessService $redisService)
    {
        $this->accessService = $redisService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tagId = $request->route('uuid');
        $accountId = $this->accessService->accountForTag($tagId);

        $id = Auth::id();

        if ($accountId !== $id) {
            throw new UnauthorizedHttpException('Tag is not assigned with this account');
        }

        return $next($request);
    }
}
