<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\AccessService;
use App\Services\DoorService;
use App\Services\LoggingService;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class TagController extends Controller
{
    use Helpers;

    /**
     * @var AccessService
     */
    private $accessService;

    /**
     * @var DoorService
     */
    private $doorService;

    /**
     * @var LoggingService
     */
    private $loggingService;

    public function __construct(
        AccessService $accessService,
        DoorService $doorService,
        LoggingService $loggingService
    ) {
        $this->accessService = $accessService;
        $this->doorService = $doorService;
        $this->loggingService = $loggingService;
    }

    /**
     * @param string $doorId
     * @param string $uuid
     * @return void
     */
    public function requestAccess(string $doorId, string $uuid): Response
    {
        $isDoorBlocked = $this->doorService->isBlocked($doorId);
        if ($isDoorBlocked) {
            $this->response->errorForbidden("Unit not available");
        }

        try {
            $access = $this->accessService->accessForTagOnDoor($doorId, $uuid);
        } catch (\Exception $exception) {
            $this->loggingService->tagNotFound($doorId, $uuid);
            return $this->response->errorNotFound($exception->getMessage());
        }

        if ($access) {
            $this->loggingService->tagSuccess($doorId, $uuid);
            return $this->response->array(["message" => "Granted", "statusCode" => 200])->statusCode(200);
        } else {
            $this->loggingService->tagDenied($doorId, $uuid);
            return $this->response->errorForbidden("Denied");
        }
    }
}
