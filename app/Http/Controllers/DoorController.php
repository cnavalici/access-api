<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\AccessService;
use App\Services\DoorService;
use App\Services\LoggingService;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

class DoorController extends Controller
{
    use Helpers;

    /**
     * @var DoorService
     */
    private $doorService;

    /**
     * @var LoggingService
     */
    private $loggingService;

    public function __construct(
        DoorService $doorService,
        LoggingService $loggingService
    ) {
        $this->doorService = $doorService;
        $this->loggingService = $loggingService;
    }

    /**
     * @return Response
     */
    public function all(): Response
    {
        $allDoors = $this->doorService->all();
        return $this->response->array($allDoors);
    }

    /**
     * @param string $doorId
     * @param int    $status
     * @return Response
     */
    public function status(string $doorId, int $status): Response
    {
        $updateStatus = ($status == 1) ?
            $this->doorService->unBlockDoor($doorId) :
            $this->doorService->blockDoor($doorId);

        $response = $this->response->noContent();
        if ($updateStatus === 0) {
            return $response->statusCode(200);
        } else {
            return $response->statusCode(201);
        }
    }
}
