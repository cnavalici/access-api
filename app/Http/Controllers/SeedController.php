<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\DoorService;
use App\Services\RedisAccessService;
use App\Services\RedisDoorService;
use Illuminate\Support\Facades\Redis;

class SeedController
{
    public function accountTags()
    {
        $tagsData = [
            // tagId => accountID (many to one)
            "4190fbc1-64b0-11eb-8bb8-3cf0112605d4" => "3ac8504f-64ae-11eb-8bb8-3cf0112605d4",
            "48efcdad-64b0-11eb-8bb8-3cf0112605d4" => "3ac8504f-64ae-11eb-8bb8-3cf0112605d4",
            "0fa0d17e-6531-11eb-8bb8-3cf0112605d4" => "3ac8504f-64ae-11eb-8bb8-3cf0112605d4",
        ];

        Redis::hMset(RedisAccessService::ACCOUNT_TAGS, $tagsData);

        return ["ok"];
    }

    public function accessData()
    {
        $door01 = [
            // tagId => 1|0
            "4190fbc1-64b0-11eb-8bb8-3cf0112605d4" => 1,
            "48efcdad-64b0-11eb-8bb8-3cf0112605d4" => 1,
            "0fa0d17e-6531-11eb-8bb8-3cf0112605d4" => 0
        ];

        $door02 = [
            "4190fbc1-64b0-11eb-8bb8-3cf0112605d4" => 0,
            "48efcdad-64b0-11eb-8bb8-3cf0112605d4" => 0,
            "0fa0d17e-6531-11eb-8bb8-3cf0112605d4" => 0
        ];

        $door03 = [
            "4190fbc1-64b0-11eb-8bb8-3cf0112605d4" => 1,
            "48efcdad-64b0-11eb-8bb8-3cf0112605d4" => 1,
            "0fa0d17e-6531-11eb-8bb8-3cf0112605d4" => 1
        ];

        Redis::hMset("door01", $door01);
        Redis::hMset("door02", $door02);
        Redis::hMset("door03", $door03);

        return ["ok"];
    }

    public function doorsData()
    {
        $doorsData = [
            "01" => DoorService::DOOR_WORKING,
            "02" => DoorService::DOOR_WORKING,
            "03" => DoorService::DOOR_BLOCKED
        ];

        Redis::hMset(RedisDoorService::DOORS_STATUSES, $doorsData);

        return ["ok"];
    }
}
