<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Welcome route - link to any public API documentation here
 */
Route::get('/', function () {
    echo "Documentation can be found here: https://app.swaggerhub.com/apis/cnav/access-api_documentation/1.0.0";
});


/** @var \Dingo\Api\Routing\Router $api */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function (Router $api) {
    /*
     * Authentication related
     */
    $api->group(['prefix' => 'auth'], function (Router $api) {
        $api->group(['prefix' => 'jwt'], function (Router $api) {
            $api->get('/token', 'App\Http\Controllers\Auth\AuthController@token');
        });
    });

    $api->group(['middleware' => ['api.auth']], function (Router $api) {
        $api->group(['prefix' => 'auth'], function (Router $api) {
            $api->group(['prefix' => 'jwt'], function (Router $api) {
                $api->get('/refresh', 'App\Http\Controllers\Auth\AuthController@refresh');
                $api->delete('/token', 'App\Http\Controllers\Auth\AuthController@logout');
            });
        });
    });

    /*
     * Authenticated routes
     */
    $api->group(['prefix' => 'access', 'middleware' => ['api.auth', 'auth.tag_is_valid']], function (Router $api) {
        $api->get('/{doorId}/{uuid}', 'App\Http\Controllers\TagController@requestAccess');
    });

    $api->group(['prefix' => 'doors', 'middleware' => ['api.auth']], function (Router $api) {
        $api->get('/', 'App\Http\Controllers\DoorController@all');
        $api->put('/{doorId}/{status}', 'App\Http\Controllers\DoorController@status');
    });

    /**
     * Helpers for REDIS dataset seeding (demo purposes)
     */
    $api->group(['prefix' => 'seed'], function (Router $api) {
        $api->post('/account-tags', 'App\Http\Controllers\SeedController@accountTags');
        $api->post('/access-data', 'App\Http\Controllers\SeedController@accessData');
        $api->post('/doors-data', 'App\Http\Controllers\SeedController@doorsData');
    });
});
